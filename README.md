# O que é o MAC Address
O **MAC Address** é o endereço físico de uma placa de rede. Um identificador único que é assignado á um **Adaptador de Rede** ou **Network Interface Controller (NIC)** em inglês. Comumente seu formato é uma sequencia de caracteres de base 16 (hexadecimais) separados por hífen, um exemplo: **b2-14-22-1f-3b-e5**

## Sua Importância no Perfect World
MAC Address é e deve ser utilizado em qualquer servidor de Perfect World. Todo administrador que se preze deve fazer o uso para conseguir manter a integridade e segurança do seu servidor contra os jogadores mal-intecionados que sempre vão usar de artíficios para esconder o IP Real que estão utilizando, seja com um Proxy ou com uma Máquina Virtual o suficientemente armada. A saída é o MAC Address. Analisar os endereços identificados aos logins efetuados em contas do jogo e poder identificar aqueles que vivem realizando atos ilícitos nos servidores e mudando seu IP constantemente para que não sejam pegos.


## Como funciona
Através do log do Perfect World gerado pelo Daemon Gacd chamado de **statinfom**, é possível filtrar e identificar os **MAC Address** de cada conta que realiza ou realizou um login no jogo.

O script possui dois modos:
#### Modo 1
Filtra todos os dados desde o começo do arquivo até o fim do arquivo e então realiza a filtragem em tempo real, ou seja, a partir de agora em diante.
#### Modo 2
Filtra apenas em tempo real, ou seja, a partir de agora em diante.

### Filtragem em tempo real:
O script começará a analisar qualquer entrada de dados no **statinfom**. A medida que o servidor roda e as contas vão logando ou deslogando, novos dados serão direcionados (output) para o **statinfom**. O script então fará a análise se a linha pertence á alguma evidência de MAC e a partir daí fará a separação dos campos importantes como: MAC Address, timestamp, userid e roleid e os importará na tabela de dados do MySQL que é fornecida junto neste projeto. 
Ou seja, tudo é feito em tempo real, a medida que novas contas logam (desde que o script esteja sendo executado), imediatamente tudo será filtrado e exportado para o MySQL.

**O objetivo único deste script é a extração dessas informações do log do Perfect World para uma tabela de dados no MySQL. Com isso é possível criar alguma aplicação ou painel standalone para poder visualizar as informações recuperadas e poder realizar tomada de decisões como: banir e desbanir contas, pesquisar por contas logadas utilizando 1 mesmo MAC Address.**


# Instalação
Para instalar você deverá setar ás variáveis de ambiente descritas na sessão ***"Variáveis de Ambiente"*** um pouco mais abaixo deste documento.

Exemplo de como setar um valor para uma variável de ambiente:

``
export PW_LOGS_WORKDIR=InformeOValorAqui
``

No exemplo acima, a variável de ambiente PW_LOGS_WORKDIR informa ao script em qual diretório está os logs do Perfect World. Então se os logs do seu PW está em: /home/pwserver/logs, ficará assim:

``
export PW_LOGS_WORKDIR=/home/pwserver/logs
``

Feito isso, configure o restante das variáveis do mesmo jeito.

**Realize uma leitura da sessão de Variáveis de Ambiente para entender cada uma delas. Essas variáveis são responsáveis por informar ao script o caminho do diretório onde está os logs do seu Perfect World e também as credenciais do seu Banco de Dados MySQL.**

Logo após ter dado o export em cada variável de ambiente com o valor correto, rode o seguinte comando:

``
./init.sh
``

O script init.sh irá configurar o arquivo config.sh com as informações que você passou para as variáveis.
Visualize o arquivo config.sh para checar se tudo foi setado corretamente.

### **Caso você tenha preguiça ou dúvida de realizar os procedimentos de configuração acima, configure o config.sh manualmente.**


## Como executar
Para utiliza-lo, faça a importação do arquivo .sql (que **está em mac_catcher/sql/mac.sql**) para a database do seu Perfect World e em seguida faça o upload de todos os arquivos deste projeto para a máquina onde está o seu Perfect World, em qualquer diretório. 

Para executa-lo, basta rodar o seguinte comando **estando dentro do diretório que você escolheu upar os arquivos do projeto**:

``
cd mac_catcher; nohup ./mac_catcher.sh start NUMERO_DO_MODO &
``

##### *Clarificando:*

##### Para o modo 1, use:
``
cd mac_catcher; nohup ./mac_catcher.sh start 1 &
``
##### Para o modo 2, use:
``
cd mac_catcher; nohup ./mac_catcher.sh start 2 &
``


## Como parar
Para parar o script basta executar:

``
./mac_catcher.sh stop
``


# Variáveis de Ambiente:
 Nome                                        | Descrição
|---------------------------------------------|-------------------------------------------------------------------------------------------|
| **PW_LOGS_WORKDIR**| Caminho completo de onde estará diretórios de logs do seu servidor de Perfect World no seu Host (geralmente o mesmo apontado/configurado em logservice.conf).
| **PW_DB_HOSTNAME**| Hostname FQDN ou IP do MySQL
| **PW_DB_PORT**| Porta do MySQL ( Padrão do MySQL: 3306 )
| **PW_DB_USER**| Usuário do MySQL que possua privilégios de acesso ao Banco de Dados escolhido
| **PW_DB_PASSWORD**| Senha do Usuário do MySQL
| **PW_DB_NAME**| Nome do Banco de Dados a ser utilizado
| **PW_DB_TABLE**| Nome da Tabela a ser utilizada
| **IS_DOCKER_INSTALL**| **Esqueça se você não usa o Docker.** Defina como true para que o script de inicialização realize a configuração e inicialização automática do serviço.

# DOCKER
Para aqueles que utilizam docker, você poderá realizar a build deste projeto, basta rodar:

``
docker build --tag pw_mac_catcher .
``

Exemplo de como executar usando as variáveis de ambiente:
```
docker run \
--name pw_mac_catcher \
-itd \
-v '/storage/pwserver/logs:/logs/' \
-e PW_LOGS_WORKDIR=/logs \
-e PW_DB_HOSTNAME=192.168.0.1 \
-e PW_DB_PORT=3306 \
-e PW_DB_USER=root\
-e PW_DB_PASSWORD=123 \
-e PW_DB_NAME=pw_db \
-e PW_DB_TABLE=mac \
-e IS_DOCKER_INSTALL=true \
pw_mac_catcher && docker network connect db pw_mac_catcher
```

Não esqueça de realizar o ***docker network connect*** para conectar o seu container mysql com o container do script, foi incluído na última linha acima no exemplo de rodagem. Utilize sempre -itd para que o connect seja realizado após o lançamento do container; no exemplo acima:

``docker network connect db pw_mac_catcher ``


# Painel para Perfect World - B2HOST e Hamaro
O painel da b2host para Perfect World feito e vendido por Hamaro tem essa funcionalidade de visualizar/gerenciar os MAC Address. Esse script veste bem e pode ser perfeitamente usado para alimentar o painel com todas as informações necessárias envolvendo MAC Address.
https://b2host.net/

![Visualização do MAC através do Painel da B2Host feito por Hamaro](http://image.prntscr.com/image/fenrHyotQkuyiIwNhCnamg.png)

# Observações e Avaliação
O projeto está rodando em um CentOS 7.6 x64, um servidor produção do Perfect World Fidelity, versão 1.5.5 sem nenhuma interrupção ou evidência de Bug's.

Apesar de simples, farei o possível para mante-lo da melhor maneira.

Feito com ❤️ por Perfect World Fidelity