#!/usr/bin/env bash

#########################################################
# Created by: Hidden (Fidelity Jogos)
# Rights: Hidden (Fidelity Jogos) & Hamaro (b2host)
# Data: 24/01/2019
# Script: Extract the data about MAC Address from PW Logs
# and import it on Data Base (MySQL).
#
# For those that want to edit this script, keep this
# commentary block. Do not remove any information above.
#########################################################

set -e


################
# Control Vars #
################

# current dir
readonly __current_dir=$(pwd)

# pid lock
readonly __pid_lockfile="/var/run/$(echo $(basename $0)).pid"


#############
# Functions #
#############

# config file
. ${__current_dir}/../config.sh

# functions
. ${__current_dir}/functions.sh


##################
# Initial Checks #
##################

# run check
if [[ -f "${__pid_lockfile}" && ! "${1}" = "stop" ]]; then
        echo "This program is already running."
        echo "You must close before launch another process."
        echo "Please, before start another proccess, type: ./$(echo $(basename $0)) stop"
        exit 2
fi


# check if mysql or mariadb is installed
command -v "mysqlimport" >/dev/null || ( echo "Error! mysql or mariadb have to be installed before to proceed."; exit 2 )

# check if pkill (procps package) is installed
command -v "pkill" >/dev/null || ( echo "Error! pkill (procps package) have to be installed before to proceed."; exit 2 )

# check if logfile exists
[[ ! -f "${__logs[statinfom]}" ]] && ( echo "Error! Log is not a valid file or it does not exists."; exit 2 )



case $1 in
        start)
		
		if [[ ! ${2} || ! ${2} =~ ^[1-2]$ ]]; then
			echo "Aborting! Necessary to pass a second parameter to inform the mode, options: (1|2)"
			exit 2
		fi

		parseMac ${__logs[statinfom]} ${2} &

          	echo "$!" > ${__pid_lockfile}
                ;;
        stop)
                if [[ -f "${__pid_lockfile}" ]]; then
			if ! (pgrep -F ${__pid_lockfile}); then
				__result=0
				clean_pid
			else
                        	echo "Exiting!"
                        	pkill -9 -P $(pgrep -F ${__pid_lockfile})
				__result=${?}
				clean_pid
			fi
                fi

                        echo "Program is not running."
                ;;
        *)
                echo "Please, inform some argument: (start|stop)"
                ;;
esac                                                

