SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for mac
-- ----------------------------
DROP TABLE IF EXISTS `mac`;
CREATE TABLE `mac`  (
  `timestamp` int(11) NOT NULL DEFAULT 0,
  `address` varchar(17) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `userid` int(20) NOT NULL,
  `roleid` int(20) NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

