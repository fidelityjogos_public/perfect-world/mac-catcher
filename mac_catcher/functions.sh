#!/usr/bin/env bash

###########
# Include #
###########
. $(pwd)'/../config.sh'


#############
# Functions #
#############

# initialy it parses the logfile and get specified data as timestamp, address, userid and roleid.
# a secure tempfile $mac_out is created to store the data then uses this information to be imported to MySQL.
parseMac() {

	# logfile fullpath
	local logpath=${1}

	# mode type
	# mode 1 = get rows since the first one and from now on (realtime filtering)
	# mode 2 = get rows from now on (realtime filtering)
	local mode=${2}


        tail --lines $( [[ "${mode}" = "1" ]] && echo +1 || echo 0 ) --follow "${logpath}" | \
	grep --ignore-case --line-buffered 'maclogin' | \
	while read -r line; do

                # parses the whole line to get only the necessary fields
                local line=$(echo ${line} | cut -d' ' -f1,2,10,11,13)

                # timestamp
                local mac_timestamp=$(date -d "$(echo "${line}" | cut -d' ' -f1-2)" +%s)

                # address
                local mac_address=$(echo "${line}" | cut -d' ' -f5 | sed -r 's/(([[:alnum:]]{2}))/\1:/g' | cut -d':' -f1-6)

                # userid
                local mac_userid=$(echo "${line}" | cut -d' ' -f3)

                # roleid
                local mac_roleid=$(echo "${line}" | cut -d' ' -f4)

                # output temp file
                local mac_out=$(mktemp /tmp/${__db[table]}.XXXXX)

                # unify the data and output it to the temp file
                echo "${mac_timestamp},${mac_address},${mac_roleid},${mac_userid}" | tee ${mac_out}

                # import data to mysql database using the temp file
                importDataToMysql ${mac_out} ${mode}
        done
}



# import a data text file with columns separated by comma to MySQL database using credentials passed by args.
importDataToMysql() {

        local db_host=${__db[hostname]}
        local db_port=${__db[port]}
        local db_user=${__db[user]}
        local db_passwd=${__db[password]}
        local db_name=${__db[name]}

        local db_file=${1}
	local mode=${2}

        local db_opts='--verbose --local --default-character-set=utf8 --fields-terminated-by=,'


	# mysql queries passed as argument
	mysql_query(){
		local query="${1}"

		if (mysql --host=${db_host} --port=${db_port} --user=${db_user} --password=${db_passwd} --execute="${query}" ${db_name}); then
			echo 0
		else
			echo 1
		fi
	}


	# test mysql connectivity
	if [[ ! ${__db[conn_stat]} = "0" ]]; then

		if [[ ! $(mysql_query 'quit') = "0" ]]; then

			echo "Aborted! Mysql connection failed! Please, check your db credentials or mysql server and try again!"
			exit 2
		else
			__db[conn_stat]=0
		fi
	fi


	# truncate table	
	if [[ ${mode} = "1" && ${__db[truncate_status]} = "1" ]]; then

		mysql_query "TRUNCATE TABLE ${__db[table]}"
		__db[truncate_status]=0
	fi


        mysqlimport \
                --host=$db_host \
                --port=$db_port \
                --user=$db_user \
                --password=$db_passwd \
		--silent \
                $(echo ${db_opts}) \
                $db_name \
                $db_file
}



clean_pid() {

	 if [[ ${__result} = "0" ]]; then
         	rm -f ${__pid_lockfile}
                clear
                echo "Successfully exited!"
                exit  0
	fi
}

