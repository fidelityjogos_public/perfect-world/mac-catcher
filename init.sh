#!/usr/bin/env bash

###########
# Include #
###########
. $(pwd)/config.sh


#############################
# ENV ENVIRONMENT INSPECTOR #
#############################
PW_DB_ENV=('HOSTNAME' 'PORT' 'USER' 'PASSWORD' 'NAME' 'TABLE')


for var in ${PW_DB_ENV[@]}; do
	declare env_var=PW_DB_${var}

	if [[ ! "${!env_var}" ]]; then
		echo "Variável de ambiente ${env_var} não inicializada!"
	fi

	sed -ri "s/(^__db\[$var\]=)(.*)/\1\'${!env_var:-null}\'/gi" config.sh
done


##################
# LOGS INSPECTOR #
##################
for log in ${__logs[@]}; do
	if [[ ! -f ${log} ]]; then
		echo "Logfile ${log} not Found! Aborting!"
		exit 2
	fi
done


if [[ "${IS_DOCKER_INSTALL}" ]]; then
	sleep 10

	# docker cmd
	cd mac_catcher; nohup ./mac_catcher.sh start ${MODE:=1} &
	
	# entrypoint
	while(true); do
		/usr/bin/env bash
		sleep 1
	done
fi

