#!/usr/bin/env bash

################
# Control Vars #
################
# cmd result set
__result=1


######################
# Perfect World Logs #
######################
__work_dir="${PW_LOGS_WORKDIR:=/home/pwserver/logs}"

declare -A __logs

__logs[statinfom]="${__work_dir}/statinfom"
__logs[w2]="${__work_dir}/world2.log"
__logs[w2_format]="${__work_dir}/world2.formatlog"


########################
# Database Credentials #
########################

declare -A __db

__db[hostname]='null'
__db[port]='null'
__db[user]='null'
__db[password]='null'
__db[name]='null'
__db[table]='null'
# script control vars
__db[conn_stat]=1
__db[truncate_status]=1
