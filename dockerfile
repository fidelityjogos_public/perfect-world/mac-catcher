FROM centos:latest

RUN yum update -y
RUN yum install -y mariadb

COPY mac_catcher/* /opt/scripts/mac_catcher/
COPY *.sh /opt/scripts/

ENTRYPOINT cd /opt/scripts; ./init.sh
